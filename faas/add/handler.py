from pymongo import MongoClient
import os
import json

def handle(req):
    """handle a request to the function
    Args:
        req (str): request body
    """
    data = json.loads(req)

    client = MongoClient('mongodb://'+os.environ.get('MONGOUSER')+':'+os.environ.get('MONGOPASS')+'@mongodb.svc-mongodb.svc.cluster.local:27017')
    #client = MongoClient('mongodb://127.0.0.1:27017')
    
    db=client.readableforever

    new_page = {
        'url' : data["url"],
        'status' : "pending"
    }
    result=db.pages.insert_one(new_page)
    
    return { 'id': str(result.inserted_id), 'status': "pending"}
