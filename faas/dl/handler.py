from pymongo import MongoClient
from bson.objectid import ObjectId
from minio import Minio
import requests
import os
import io
import json
import pprint
import base64

import base64
from uuid import uuid4
from elasticsearch import Elasticsearch


def handle(req):
    """handle a request to the function
    Args:
        req (str): request body
    """
    data = json.loads(req)
    id = data["id"]
    print(id)
    print("-----------------")
    client = MongoClient('mongodb://'+os.environ.get('MONGOUSER')+':'+os.environ.get('MONGOPASS')+'@mongodb.svc-mongodb.svc.cluster.local:27017')
#    client = MongoClient('mongodb://127.0.0.1:27017')
    db=client.readableforever

    res = db.pages.find_one( { "_id": ObjectId(id) }, { "_id": 0 } )

    print(res["url"])
    print(res["status"])
    pload = {'url': res["url"]}
    r =requests.post( "http://singlefile.default.svc.cluster.local/",data = pload)
    #r =requests.post( "http://127.0.0.1:9000/",data = pload)

    print(r.headers["Content-Length"])

    client = Minio("minio.svc-minio.svc.cluster.local:9000", "ODDy2toTLCnt3PZ09Vxn", "BfM2bIL3MJYmCRKLUwRmt7kLewV985stPzCO3A5l", secure=0)

    value_as_bytes = r.text.encode('utf-8')
    value_as_a_stream = io.BytesIO(value_as_bytes)

    result = client.put_object(
        "pages", id+".html", value_as_a_stream, length=len(value_as_bytes),
    )


    newvalues = { "$set": { "status": "downloaded" } }
    res = db.pages.update( { "_id": ObjectId(id) }, newvalues )

    plain_string = r.text
    bytes_string = bytes(plain_string, 'utf-8')
    b64 = base64.b64encode(bytes_string)

    tmp =  b64.decode()

    ploadb64 = {"data": tmp}

    print(ploadb64)

    #elastic_client = Elasticsearch(hosts=["elasticsearch-api.k0s.freephp5.net"], scheme="http", port=80)
    elastic_client = Elasticsearch(hosts=["elasticsearch.svc-elasticsearch.svc.cluster.local"], scheme="http", port=9200)
    
    result = elastic_client.index(index="readableforever", doc_type="_doc", id=id, body=ploadb64, pipeline='attachment')

#    r =requests.put( "http://elasticsearch.svc-elasticsearch.svc.cluster.local:9200/readableforever/_doc/"+id,data = ploadb64)
    #r =requests.put( "http://elasticsearch-api.k0s.freephp5.net/readableforever/_doc/"+id,data = ploadb64,headers=headers)

    return result

